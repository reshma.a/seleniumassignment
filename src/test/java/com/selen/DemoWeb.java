package com.selen;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWeb {

	public static void main(String[] args) throws InterruptedException {
WebDriver driver = new ChromeDriver();
driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
driver.get("https://demowebshop.tricentis.com/");
driver.findElement(By.xpath("(//a[contains(text(),'Log in')])")).click();
driver.findElement(By.xpath("(//input[@id=\"Email\"])")).sendKeys("chingumingureshu@gmail.com");
driver.findElement(By.xpath("(//input[@id=\"Password\"])")).sendKeys("Chingumingu@123");
driver.findElement(By.xpath("(//input[@type=\"submit\"])[2]")).click();
Actions a = new Actions(driver);
WebElement computer = driver.findElement(By.xpath("(//a[contains(text(),'Computers')])[1]"));
a.moveToElement(computer).perform();
WebElement desktop = driver.findElement(By.xpath("(//a[contains(text(),'Desktops')])[1]"));
a.moveToElement(desktop).click().perform();
WebElement sel = driver.findElement(By.xpath("//select[@id='products-orderby']"));
Select s = new Select(sel);
s.selectByVisibleText("Price: Low to High");
driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();

driver.findElement(By.xpath("(//input[@id='add-to-cart-button-72'])")).click();
Thread.sleep(5000);
driver.findElement(By.xpath("(//span[contains(text(),'Shopping cart')])")).click();

driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click();
driver.findElement(By.xpath("(//button[@id='checkout'])")).click();
WebElement newadress = driver.findElement(By.xpath("//select[@id='billing-address-select']"));
Select sss=new Select(newadress);
sss.selectByVisibleText("New Address");
WebElement country = driver.findElement(By.xpath("//select[@id='BillingNewAddress_CountryId']"));
Select ss= new Select(country);
ss.selectByVisibleText("India");
driver.findElement(By.xpath("//input[@id='BillingNewAddress_City']")).sendKeys("Coimbatore");
driver.findElement(By.xpath("//input[@id='BillingNewAddress_Address1']")).sendKeys("R.ponnapuram pollachi");
driver.findElement(By.xpath("//input[@id='BillingNewAddress_ZipPostalCode']")).sendKeys("642002");
driver.findElement(By.xpath("//input[@id='BillingNewAddress_PhoneNumber']")).sendKeys("9345497282");
driver.findElement(By.xpath("(//input[@title='Continue'])[1]")).click();
driver.findElement(By.xpath("(//input[@title='Continue'])[2]")).click();
driver.findElement(By.xpath("(//input[@class='button-1 shipping-method-next-step-button'])")).click();
driver.findElement(By.xpath("(//input[@class='button-1 payment-method-next-step-button'])")).click();
driver.findElement(By.xpath("(//input[@class='button-1 payment-info-next-step-button'])")).click();
driver.findElement(By.xpath("(//input[@class='button-1 confirm-order-next-step-button'])")).click();
System.out.println("==Hurrey order placed==");

	}

}
